# jupyter-notebook-output-for-reader README


**jupyter-notebook-output-for-reader (for NVDA)**

**by Ziyi Lin and Fumi.Iseki**

## Features

This is a **VSCode extension** for the visually impaired to use **NVDA** (screen reading software).

When using **ipynb files (Jupyter)** with VSCode, the cursor does not move to the result of the execution and the result cannot be read out loud.

This plugin allows the result part of the ipynb execution to be displayed as a separate text tab. 

It is also possible to insert the results as a comment at the end of the ipynb code cell.

## Usage
#### Jupyter-output.openAsFile
- Short cut key : **ctrl + alt + c**

Opens another text tab and displays the results of the run.

The cursor moves to the first line (blank line).
1. excute code cell (ctrl + enter)
1. ctrl + alt + c
1. down coursol

#### Jupyter-output.appendToCell
- Short cut key : **ctrl + alt + a**

Inserts the results as a comment at the end of the ipynb code cell.

The cursor moves to the first comment line (# and spaces line).
1. excute code cell (ctrl + enter)
1. ctrl + alt + a
1. enter
1. down coursol

## Release Notes

### 0.1.0
2 May 2023

Core ideas and implementation by Ziyi Lin

### 0.2.0
8 May 2023

Fixed a bug that some messages are not displayed by Ziyi Lin

### 0.3.0
16 May 2023 

Little modifications and localize by Fumi.Iseki 

## Build
```
> git clone https://gitlab.nsl.tuis.ac.jp/iseki/jupyter-notebook-output-for-reader.git
> cd jupyter-notebook-output-for-reader
> npm i             (download node_modules)
> npx vsce package  (or  code .)
```
## Binary Download
- [jupyter-notebook-output-for-reader-0.3.6.vsix](https://www.nsl.tuis.ac.jp/DownLoad/SoftWare/VSCode/extensions/jupyter-notebook-output-for-reader-0.3.6.vsix)

