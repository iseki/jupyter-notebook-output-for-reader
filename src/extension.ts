// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { l10n } from 'vscode';

// do not delete spaces
let appendStartLine = "#                                                  ";

//
function excludeCurrentAppendedText(cell: vscode.NotebookCell | undefined) {
    if (!cell) { return; }

    let originalText = cell.document.getText();
    let originTextArr = originalText.split(/\r?\n/);
    
    let outputIndex = originTextArr.findIndex(line => line.includes(appendStartLine));
    if (outputIndex === -1) { return originalText; }
    
    let newText = '';
    if (outputIndex !== -1) {
        newText = originTextArr.slice(0, outputIndex-1).join('\r\n');
    }

    return newText;
}


//
async function clearCurrentAppendedText(cell: vscode.NotebookCell | undefined) {
    if (!cell) { return; }

    const newText = excludeCurrentAppendedText(cell);
    
    // replace the cell's content
    const edit = new vscode.WorkspaceEdit();
    edit.replace(cell.document.uri, new vscode.Range(0, 0, cell.document.lineCount, 0), newText as string);
    await vscode.workspace.applyEdit(edit);
}


interface CellData {
    originalText: string;
    output: string[];
    error: {errName: string, errMsg: string}[];
}


//
function getCellData(cell: vscode.NotebookCell | undefined) {
    if (!cell) { return; }

    const cellData: CellData = {
        originalText: cell.document.getText(),
        output: [] as string[],
        error: [] as {errName: string, errMsg: string}[]
    };

    const selectedCellOutputs = cell.outputs;

    for (const output of selectedCellOutputs || []) {
        for (const item of output.items) {
            if (item.mime === 'text/plain' || item.mime === 'application/vnd.code.notebook.stdout'
                                           || item.mime === 'application/vnd.code.notebook.stderr') {
                cellData.output.push(item.data.toString());
            }
            if (item.mime.includes('error')) {
                const err = JSON.parse(item.data.toString());
                cellData.error.push({errName: err.name, errMsg: err.message});
            }
        }
    }

    return cellData;
}


// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
// memo: npx vsce package
export function activate(context: vscode.ExtensionContext) {
    //
    context.subscriptions.push(vscode.commands.registerCommand('jupyter-notebook-output-for-reader.appendToCell', async () => {
        const activeEditor = vscode.window.activeTextEditor;
        if (!activeEditor) {
              vscode.window.showErrorMessage('No active editor');
              return;
        }
        const cells = vscode.window.activeNotebookEditor?.notebook.getCells();
        
        // Get the selected cell
        const selectedCellText = activeEditor.document.getText();
        const selectedCell = cells?.find(cell => cell.document.getText() === selectedCellText);

        await clearCurrentAppendedText(selectedCell);
        const cellData: (CellData | undefined) = getCellData(selectedCell);
        if (!cellData) { return; }
        
        let appendText = '';
        if (cellData.output.length > 0) {
            appendText += '# ' + l10n.t('the following is output') + '\r\n';
            for (const output of cellData.output) {
                appendText = appendText + '# ' + output.replace(/\n/g, '\n# ') + '\r\n';
            }
        }
        if (cellData.error.length > 0) {
            appendText += '# ' + l10n.t('error occurred') + '\r\n';
            for (const err of cellData.error) {
                appendText += '# ' + l10n.t('the error type is ') + err.errName + '\r\n';
                appendText += '# ' + l10n.t('the error message is ') + err.errMsg + '\r\n';
            }
        }
        else {
            if (appendText==='') { appendText = '# ' + l10n.t('no output') + '\r\n';}
            appendText = '# ' + l10n.t('no error') + '\r\n' + appendText;
        }
        appendText += '# ' + l10n.t('that is all');
        appendText = "\r\n\r\n" + appendStartLine + "\r\n" + appendText;

        let lineCount = activeEditor.document.lineCount;
        // append output to the end of the cell
        const edit = new vscode.WorkspaceEdit();
        edit.insert(activeEditor.document.uri, new vscode.Position(activeEditor.document.lineCount, 0), appendText);
        await vscode.workspace.applyEdit(edit);	
        //
        activeEditor.selection = new vscode.Selection(new vscode.Position(lineCount+1, 0), new vscode.Position(lineCount+1,0));
    }));

    //
    context.subscriptions.push(vscode.commands.registerCommand('jupyter-notebook-output-for-reader.openAsFile', async () => {
        const activeEditor = vscode.window.activeTextEditor;
        if (!activeEditor) {
              vscode.window.showErrorMessage('No active editor');
              return;
        }
        const cells = vscode.window.activeNotebookEditor?.notebook.getCells();
        
        // Get the selected cell's output
        let selectedCellText = activeEditor.document.getText();
        const selectedCell = cells?.find(cell => cell.document.getText() === selectedCellText);
        
        await clearCurrentAppendedText(selectedCell);
        const cellData: (CellData | undefined) = getCellData(selectedCell);
        if (!cellData) { return; }

        let outputText = '';
        if (cellData.output.length > 0) {
            outputText = l10n.t('the following is output') + '\r\n';
            for (const output of cellData.output) {
                outputText += output + '\r\n';
            }
        }
        if (cellData.error.length > 0) {
            outputText += '\r\n' + l10n.t('error occurred') + '\r\n';
            for (const err of cellData.error) {
                outputText += l10n.t('the error type is ') + err.errName + '\r\n';
                outputText += l10n.t('the error message is ') + err.errMsg + '\r\n';
            }
        }
        else {
            if (outputText==='') { outputText = l10n.t('no output') + '\r\n';}
            outputText = '\r\n' + l10n.t('no error') + '\r\n' + outputText;
        }
        outputText += l10n.t('that is all');
        
        // Open a new file, fill with content
        const language = 'plaintext';
        const uri = vscode.Uri.parse(`untitled:jupyter output`);

        vscode.workspace.openTextDocument(uri)
        .then(doc => {
            vscode.window.showTextDocument(doc, 1, false).then(e => {
                e.edit(edit => {
                    // clear the document
                    edit.delete(new vscode.Range(new vscode.Position(0, 0), new vscode.Position(doc.lineCount + 1, 0)));
                    // insert the content
                    edit.insert(new vscode.Position(0, 0), outputText);
                });
                e.selection = new vscode.Selection(new vscode.Position(0, 0), new vscode.Position(0, 0));
            });
        });
    }));
}

// This method is called when your extension is deactivated
export function deactivate() {}
